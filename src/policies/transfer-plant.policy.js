const bodyParser = require('body-parser');
const db = require('./../db');
const joi = require('joi');

const schema = joi.object().keys({
    username: joi.string().regex(/^[A-Za-z\-\_0-9\S]{3,20}$/).required(),
    plant_id: joi.string().required()
});

module.exports = function (req, res, next) {
    const dataToValidate = req.body;
    joi.validate(dataToValidate, schema, (err, value) => {
        if (err) return res.status(400).send({ message: 'Invalid input!' });
        next();
    });
}