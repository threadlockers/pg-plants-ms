const db = require('../db');

module.exports.displayAvailable = function (callback) {
    db.query('SELECT * FROM `Plants` WHERE inventory_id IS NULL ', function (err, result) {
        if (err) return callback(err);
        callback(null, result);
    });
};

module.exports.changeOwnership = function (data, callback) {

    let values = [
        data.username
    ]

    db.query('SELECT * FROM `user` WHERE username=?', values, function (err, result) {
        values = [
            result[0].inventory_id,
            data.plant_id
        ]

        let credits = result[0].credits;

        db.query('SELECT * FROM `Plants` WHERE plant_id=?', [data.plant_id], (err, result) => {
            if (err) return callback(err);

            let price = result[0].price;

            if (credits >= price) {
                db.query('UPDATE `Plants` SET inventory_id=? WHERE plant_id=?', values, (err) => {
                    if (err) return callback(err);

                    const remainingCredits = credits - price;

                    db.query('UPDATE `user` SET credits=? WHERE username=?',
                        [remainingCredits, data.username],
                        (err, result) => {
                            if (err) callback(err);
                            callback(null, result);
                        });
                });
            }
            else
                callback(null, { message: 'Insufficient funds!' });
        });

    });
};

module.exports.viewUsersPlants = function (data, callback) {

    let values = [
        data.username
    ];

    db.query('SELECT * FROM `user` NATURAL JOIN `Plants`WHERE username=?', values, function (err, result) {
        if (err) return callback(err);
        let parsedResult = result.map((element) => {
            return {
                type: element.type,
                age: element.age,
                effects: element.effects,
                livestream_domain: element.livestream_domain,
                livestream_port: element.livestream_port,
                plant_id: element.plant_id,
            };
        });
        callback(null, parsedResult);
    });
};

module.exports.getPlantData = function (plant_id, callback) {
    db.query('SELECT * FROM `Plants` WHERE plant_id=?', [plant_id], (err, result) => {
        if(err) return callback(err);
        return callback(null, result[0]);
    });
}