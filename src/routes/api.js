const express = require('express');
const router = express.Router();
const db = require('../db');
const jwt_controller = require('../controllers/jwt.controller');
const Plants = require('../models/plants');
const bodyParser = require('body-parser');
const ownedPolicy = require('../policies/owned.policy');
const transferPlantPolicy = require('../policies/transfer-plant.policy');

router.post('/available', jwt_controller, (req, res) => {
    Plants.displayAvailable((err, result) => {
        if(err) return res.status(500).send({message: 'Something went wrong!'});
        return res.status(200).send(result);
    });
});

router.post('/transfer', jwt_controller, transferPlantPolicy, (req, res) => {
    
    let queryObject = {
        username: req.body.username,
        plant_id: req.body.plant_id,
    };
    
    Plants.changeOwnership(queryObject, (err, result) => {
        if(err) return res.status(500).send({message: 'Something went wrong!'});
        return res.status(200).send({message: `Plant was transferred sucessfully to ${req.body.username}`});
    })
});

router.post('/owned', jwt_controller, ownedPolicy, (req,res) => {

    let queryObject = {
        username: req.body.username,
    };    

    Plants.viewUsersPlants(queryObject, (err, result) => {
        if(err) return res.status(500).send({message: 'Something went wrong!'});
        return res.status(200).send(result);
    });    
});

router.post('/plant-data', jwt_controller, (req, res) => {
    plant_id = req.body.plant_id;
    Plants.getPlantData(plant_id, (err, result) => {
        if (err) return res.status(500).send({message: 'Internal error!'});
        return res.status(200).send(result);
    })
});

module.exports = router;